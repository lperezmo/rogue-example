#include <rogue/Logging.h>
#include <rogue/protocols/udp/Core.h>
#include <rogue/protocols/udp/Client.h>
#include <rogue/protocols/rssi/Client.h>
#include <rogue/protocols/rssi/Transport.h>
#include <rogue/protocols/rssi/Application.h>
#include <rogue/protocols/packetizer/CoreV2.h>
#include <rogue/protocols/packetizer/Transport.h>
#include <rogue/protocols/packetizer/Application.h>
#include <rogue/protocols/srp/SrpV3.h>
#include <rogue/interfaces/memory/Master.h>
#include <rogue/interfaces/memory/Constants.h>
#include <rogue/utilities/fileio/StreamReader.h>

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;
namespace ruf = rogue::utilities::fileio;

int main (int argc, char **argv) {

// ----------------------------------------------------------------------------------------
//!--  Parameters defined by User
// ----------------------------------------------------------------------------------------
   const uint32_t  MAX_ADDR_MEM = 16384;       // Number of Words  in BRAM (128k/8)
   const uint32_t  INCR_ADDR_VALUE = 8;        // Word size in bytes
   const int MAX_NUM = 16384;                  // Memory size in words 
   const int WR_ADDR_INCR= 512;                // (4096-byte/8-byte) = (maxSize/INCR_ADDR_VALUE)
   const int LOOP_NUM = 100000;                     //
   int MEMORY_BLOCK_S[4]; 
       MEMORY_BLOCK_S[0] = 32;                 // Full 128Kbyte BRAM (64-bit word)
       MEMORY_BLOCK_S[1] = 1;                  // 1 4096 Block
       MEMORY_BLOCK_S[2] = 16;                 // Half BRAM 
       MEMORY_BLOCK_S[3] = 2;                  // 2 4096 Block
   const int MEMORY_BLOCK=MEMORY_BLOCK_S[0];   // Defined by User 
// ---------------------------------------------------------------------------------------------
//! Base Addres
// ---------------------------------------------------------------------------------------------
   uint64_t addr_mem=0x0000000041000000;
   uint64_t addr_mem_read=0x0000000041000000;
   uint64_t base_addr=0x0000000041000000;
   uint64_t base_addr_read=0x0000000041000000;  
// ---------------------------------------------------------------------------------------------
 
   uint64_t mem[MAX_ADDR_MEM];
   uint64_t memR[MAX_ADDR_MEM];

   uint32_t maxSize; 
   uint32_t minSize; 
   uint64_t maxPayLoad;

   ifstream inFile; 
   ofstream outFile; 
   int data_gen = 3;
   int  indx;
   uint32_t  tmp;
   uint32_t  tmp3;
   int  tmp2;
   uint64_t value_data[MAX_NUM+1];
   
   //rogue::Logging::setLevel(rogue::Logging::Debug);

// ----------------------------------------------------------------------------------------
//! Create the UDP client, jumbo = true
// ----------------------------------------------------------------------------------------
   rogue::protocols::udp::ClientPtr udp  = rogue::protocols::udp::Client::create("172.30.0.128",8192,true);
   udp->setRxBufferCount(64); // Make enough room for 64 outstanding buffers

// ----------------------------------------------------------------------------------------
//! RSSI
// ----------------------------------------------------------------------------------------
   rogue::protocols::rssi::ClientPtr rssi = rogue::protocols::rssi::Client::create(udp->maxPayload());
   udp->addSlave(rssi->transport());
   rssi->transport()->addSlave(udp);

// ----------------------------------------------------------------------------------------
//! Packetizer, ibCrc = false, obCrc = true
// ----------------------------------------------------------------------------------------
   rogue::protocols::packetizer::CoreV2Ptr pack = rogue::protocols::packetizer::CoreV2::create(false,true,true);
   rssi->application()->addSlave(pack->transport());
   pack->transport()->addSlave(rssi->application());
// ----------------------------------------------------------------------------------------
//! Create an SRP master and connect it to the packetizer
// ----------------------------------------------------------------------------------------
   rogue::protocols::srp::SrpV3Ptr srp = rogue::protocols::srp::SrpV3::create();
   pack->application(0)->addSlave(srp);
   srp->addSlave(pack->application(0));

// ----------------------------------------------------------------------------------------
//! Create a memory master and connect it to the srp
// ----------------------------------------------------------------------------------------
   rogue::interfaces::memory::MasterPtr mast = rogue::interfaces::memory::Master::create ();
   mast->setSlave(srp);
// ----------------------------------------------------------------------------------------
//! Read file and  create Read Stream class.  
// ----------------------------------------------------------------------------------------
  //rogue::utilities::fileio::StreamReaderPtr Fread = rogue::utilities::fileio::StreamReader::create();
 // Fread->open("/scratch/lpmoreno/git/rogue/build/rogue-example/cpp_register_access/bin/mat4.dat");
 // Fread->isOpen();
 // Fread->closeWait();
//! Initialize indx 
   indx=0; 
// ----------------------------------------------------------------------------------------
//! Standard read file process 
// ----------------------------------------------------------------------------------------
  if (argc > 1) {
    printf("Argc Value %d\n",argc);
     data_gen = atoi(argv[1]);
  } else {
    printf("Argc Value %d\n",argc);
    data_gen = 3;
  }
  
  if (data_gen < 2) {
    printf ("Reading file ...\n");
   inFile.open("/scratch/lpmoreno/git/rogue/build/rogue-example/cpp_register_access/bin/mat4.dat");
   while(1) 
    {
     if (!inFile)
      {
      printf("Error reading File\n");
      exit (1);     
    }
     if (indx > MAX_NUM) break;
      {
        inFile >> tmp2;
        value_data[indx] = tmp2;
       // printf("Monitor indc %d  value = %d \n",indx,tmp2);
        indx++;
       }
     }
      inFile.close();
    } else if (data_gen < 3){
     printf("Generating random data ..\n");
// ---------------------------------------------------------------------------------------------
//!  Random  Data 
// ---------------------------------------------------------------------------------------------
    for (int indx=0; indx < MAX_ADDR_MEM; indx ++)
      {
       value_data[indx]=rand() % 100000 + 1;
       memR[indx]=0x44444444FFFFFFFF;
      // value_data[indx]=indx;
        }
    } else {
// ---------------------------------------------------------------------------------------------
//!  Fix Pattern
// ---------------------------------------------------------------------------------------------
    printf("Constant data ..\n");
    for (int indx=0; indx < MAX_ADDR_MEM; indx ++)
      {
       value_data[indx]=1122334455667788;
       memR[indx]=0x44444444FFFFFFFF;
      // value_data[indx]=indx;
        }
    }
// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
//! Start the RSSI connection
// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
   rssi->start();

   while ( ! rssi->getOpen() ) {
      sleep(1);
      printf("Establishing link ...\n");
   }
// ---------------------------------------------------------------------------------------------
     
// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
//! Get SW parameters. 
// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
  maxPayLoad=udp->maxPayload();
  printf("Max Payload %d\n",maxPayLoad);
  minSize= mast->reqMinAccess();
  printf("Minmum  Access %d bytes \n",minSize);
  maxSize= mast->reqMaxAccess();
  printf("Maximum Access %d bytes \n",maxSize);
  printf("W  R  I  T  I  N   G .. \n");
// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
//! Write  Registers --- ROGUE --> FPGA 
// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
  for (int idx=0; idx < LOOP_NUM ; idx ++)
  {
   for(int indxw=0; indxw < MEMORY_BLOCK; indxw ++ )
      {
       addr_mem= base_addr+(indxw*maxSize);
       mast->reqTransaction(addr_mem,maxSize,&value_data[indxw*WR_ADDR_INCR],rogue::interfaces::memory::Write);
       //printf("Spy  Address_Write =0x%x, Data_Write=0x%llx \n",addr_mem,value_data[indxw]);
     }
      mast->waitTransaction(0);
  }
    printf("Write full  memory --> Done \n");
// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
//! Read Registers -- ROGUE <-- FPGA 
// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
  printf("R  E  A  D  I  N  G  .. \n");
    for (int  indxr=0; indxr < WR_ADDR_INCR*MEMORY_BLOCK; indxr ++)
     {
     addr_mem_read=base_addr_read+(indxr*INCR_ADDR_VALUE );
     mast->reqTransaction(addr_mem_read,8,&memR[indxr],rogue::interfaces::memory::Read);
     mast->waitTransaction(0);
    // printf("Spy  Address_Read =0x%x, Data_Read=0x%llx \n",addr_mem_read,memR[indxr]);
     }
    mast->waitTransaction(0);
  printf("Read full memory --> Done \n");
/// ----------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
//! Write file
// ----------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
    outFile.open("/scratch/lpmoreno/git/rogue/build/rogue-example/cpp_register_access/bin/outmat4.dat");
    printf("Writing resutl file....\n");
  for (int indxwr=0; indxwr <WR_ADDR_INCR*MEMORY_BLOCK; indxwr ++) 
      {
       outFile << memR[indxwr] << endl;
      // printf(" spy indwr = %d value 0x%x \n", indxwr, memR[indxwr]);
      } 
    outFile.close();
   printf("Write file -- done!\n");
// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
//!  Results 
// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
//  printf("MemR[0]: 0x%x, MemR[1]: 0x%x. MemR[2]: 0x%x\n",memR[0], memR[3], memR[8]);
//
//// Compare result 
 for (int indxc =0; indxc < WR_ADDR_INCR*MEMORY_BLOCK; indxc ++)
  {
    // printf("Spy  Data_Write =0x%x, Data_Read =0x%x \n",mem[indxc],memR[indxc]);
     if (value_data[indxc] != memR[indxc]) 
       {
         printf("Error detected in addr = 0x%x  Data_W =0x%x Data_R +0x%x\n",indxc, value_data[indxc], memR[indxc]);  
      }    
  }
}
